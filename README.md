# Arduino Clock

This repository includes an arduino sketch and instructions for an LED clock made with an arduino. These instructions were made for the Spring 2015 NCSU ENG 331 course.

The arduino code requires the [Time](https://github.com/PaulStoffregen/Time) Arduino library by [Paul Stoffregen](https://github.com/PaulStoffregen)
